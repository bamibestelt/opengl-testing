/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zoom.opengl;

import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.util.Log;

/**
 * Provides drawing instructions for a GLSurfaceView object. This class
 * must override the OpenGL ES drawing lifecycle methods:
 * <ul>
 *   <li>{@link android.opengl.GLSurfaceView.Renderer#onSurfaceCreated}</li>
 *   <li>{@link android.opengl.GLSurfaceView.Renderer#onDrawFrame}</li>
 *   <li>{@link android.opengl.GLSurfaceView.Renderer#onSurfaceChanged}</li>
 * </ul>
 */
public class MyGLRenderer implements GLSurfaceView.Renderer {

    //private Triangle mTriangle;
    //private Line mLine;
    private boolean isFirst = false;
	
    private float mAngle;
    private float factor = 1.0f;

    private int WIDTH;
    private int HEIGHT;
    private MyGLSurfaceView view;
    /*
    private float lineCoords[] = {
        // in counterclockwise order:
        0.0f,  0.0f, 0.0f,// top
        0.0f, 0.0f, 0.0f// bottom left
    };
    */
    
    private ArrayList<Line> listOfArrays = new ArrayList<Line>();
    private ArrayList<Line> bufferOfArrays = new ArrayList<Line>();
    
    public MyGLRenderer(MyGLSurfaceView myGLSurfaceView) {
		// TODO Auto-generated constructor stub
    	view = myGLSurfaceView;
	}

	@Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        // Set the background frame color
        gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        //mTriangle = new Triangle();
        //mLine = new Line();
    }

    @Override
    public void onDrawFrame(GL10 gl) {
    	//Log.v("Renderer", "onDrawFrame");
    	
        // Draw background color
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

        // Set GL_MODELVIEW transformation mode
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();   // reset the matrix to its default state

        // When using GL_MODELVIEW, you must set the view point
        GLU.gluLookAt(gl, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);

        gl.glScalef(factor, factor, 0.0f);

        // Draw triangle
        //mTriangle.draw(gl);
        //TODO mLine.draw(gl);
        
        for(Line line : listOfArrays) {
        	line.draw(gl);
        }
        isFirst = true;
        
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        // Adjust the viewport based on geometry changes
        // such as screen rotations
        gl.glViewport(0, 0, width, height);
        WIDTH = width;
        HEIGHT = height;
        Log.d("Renderer", "width: "+WIDTH+", height: "+HEIGHT);
        
        // make adjustments for screen ratio
        float ratio = (float) width / height;
        gl.glMatrixMode(GL10.GL_PROJECTION);        // set matrix to projection mode
        gl.glLoadIdentity();                        // reset the matrix to its default state
        gl.glFrustumf(-ratio, ratio, -1, 1, 3, 7);  // apply the projection matrix
    }

    /**
     * Returns the rotation angle of the triangle shape (mTriangle).
     *
     * @return - A float representing the rotation angle.
     */
    public float getAngle() {
        return mAngle;
    }

    /**
     * Sets the rotation angle of the triangle shape (mTriangle).
     */
    public void setAngle(float angle) {
        mAngle = angle;
    }

	public void setScaleVertice(float mScaleFactor) {
		factor = mScaleFactor;
	}

	public void setLineCoordinates(float mPreviousX
			, float mPreviousY, float x, float y) {
		// TODO Auto-generated method stub
		
		float lineCoords[] = new float[6];
		lineCoords[0] = (float) (mPreviousX * 2.0 / WIDTH - 1.0);
		lineCoords[1] = (float) (mPreviousY * -2.0 / HEIGHT + 1.0);
		lineCoords[2] = 0.0f;
		lineCoords[3] = (float) (x * 2.0 / WIDTH - 1.0);
		lineCoords[4] = (float) (y * -2.0 / HEIGHT + 1.0);
		lineCoords[5] = 0.0f;
		
		bufferOfArrays.add(new Line(lineCoords));
		
		if(isFirst) {
			isFirst = false;
			listOfArrays.addAll(bufferOfArrays);
			view.requestRender();
		}
		
	}

	public void setFirst(boolean isFirst) {
		this.isFirst = isFirst;
	}
	
	
}